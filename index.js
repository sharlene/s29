// find s in firstName or d in lastName

db.users.find(
    {$or:[
        {firstName: {$regex: "s", $options: "i"},},
        {lastName: {$regex: "d", $options: "i"},}
        ]
    },
        
    {firstName: 1, lastName: 1, _id: 0}
);


// find users who are from HR dept. and thier are is greater that or equal to 70

db.users.find({$and: [
    {department: "HR"}, 
    {age: {$gte: 70}}
]});

//find users with letter e in their first name and has an age of less that or equal to 30

db.users.find({$and: [
    {firstName:{$regex: "e", $options: "i"},},
    {age: {$lte: 30}}
]});